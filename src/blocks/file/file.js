$('.file__input').on("change", function () {
  var filename = $(this).val().replace(/C:\\fakepath\\/i, '');
  $(this).closest('.form__file-wrapper').find('.form__file').text(filename);
});