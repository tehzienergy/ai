if ($('.sAntiSanctionsMeasures__row > *').length > 4) {
  $('.sAntiSanctionsMeasures__row').slick({
    slidesToShow: 4,
    arrows: false,
    dots: true,
    infinite: false,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3,
          adaptiveHeight: true
        }
      },
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2,
          adaptiveHeight: true
        }
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 1,
          adaptiveHeight: true
        }
      },
    ]
  });
}